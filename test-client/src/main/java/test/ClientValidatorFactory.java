package test;

import javax.validation.Validator;
import com.google.gwt.core.client.GWT;

import de.knightsoftnet.validators.client.AbstractGwtValidatorFactory;
import de.knightsoftnet.validators.client.GwtValidation;
import de.knightsoftnet.validators.client.impl.AbstractGwtValidator;

public class ClientValidatorFactory extends AbstractGwtValidatorFactory {
    @GwtValidation(value = {ValidTest.class, ValidTest2.class})
    interface ClientValidator extends Validator { }

    @Override
    public AbstractGwtValidator createValidator() {
        // return GWT.create(ClientValidator.class);
        return new ClientValidatorImpl();
    }
}