package test;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MyValidationValidator implements ConstraintValidator<MyValidation, Object> {

    @Override
    public void initialize(final MyValidation constraintAnnotation) {
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        return false;
    }
}
